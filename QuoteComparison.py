import pandas as pd 
import numpy as np

class QuoteComparison(object):
    def __init__(self,quote_dict):
        self.quotes = quote_dict
        
    @staticmethod
    def hashThis(inverter=None,pv=None,contractor=None,utility=None):
        return str(inverter) + "_X_" + str(pv) + "_X_" + str(contractor) + str(utility)
    
    @staticmethod
    def unHashThis(hsh):
        return hsh.split("_X_")
            
    def getBestRate(self,power):
        best = np.inf
        best_key = ""
        for key,value in self.quotes.items():
            if value.getQuote(power)[0] < best:
                best = value.getQuote(power)[0]
                best_key = key
        return best,QuoteComparison.unHashThis(key)
    
    
    #This actually turned into a nightmare and should be re-factored. 
    #It all has to be unpacked for the sorting. Then re-packed. 
    def returnRankedRates(self,power,asym_conf_int=True,error_filter=0.2):
        flat_dict = {}
        flat_errors_up = {}
        flat_errors_down = {}
        flat_sys_errors_up = {}
        flat_sys_errors_down = {}
        
        flat_naive_rate_mean = {}
        flat_naive_rate_std = {}
        
        flat_typical_power_mean = {}
        flat_typical_power_std = {}

        flat_is_multilinear = {}
        flat_is_microinverter = {}

        for key,value in self.quotes.items():
            
            flat_dict[key], flat_errors_up[key], flat_errors_down[key] = value.getQuote(power,asym_conf_int)
            flat_naive_rate_mean[key], flat_naive_rate_std[key] = value.getNaiveRate()
            flat_typical_power_mean[key], flat_typical_power_std[key] = value.getTypicalPower()
#             value.setSysError(0.17)
#             flat_sys_errors_up[key], flat_sys_errors_down[key] = value.getSysError()
            flat_is_multilinear[key] = value.is_multilinear
            flat_is_microinverter[key] = value.is_microinverter
            
#             flat_errors[key] = value.getQuote(power)[1]
            
        sorted_tuple = sorted(flat_dict.items(), key=lambda x: x[1])
        
        df = pd.DataFrame()
        
        
        df["cost_for_%skW"%power] = [ x[1] for x in sorted_tuple ]
        
        dollars_per_watt = lambda x: (x *1000000)/(power*1000)
        df["dollar_per_watt"] = np.array([ dollars_per_watt(float(x[1])) for x in sorted_tuple ])#.map(lambda x: (x *1000000)/(power*1000) )
        df["errorUp_on_cost"] = [ flat_errors_up[x[0]] for x in sorted_tuple ]
        df["errorDown_on_cost"] = [ flat_errors_down[x[0]] for x in sorted_tuple ]
#         df["sys_errorUp_annual_sunlight"] = [ flat_sys_errors_up[x[0]] for x in sorted_tuple ]
#         df["sys_errorDown_annual_sunlight"] = [ flat_sys_errors_down[x[0]] for x in sorted_tuple ]
        
        df["naive_M$/kW"] = [ flat_naive_rate_mean[x[0]] for x in sorted_tuple ]
        df["naive_M$/kW_error"] = [ flat_naive_rate_std[x[0]] for x in sorted_tuple ]
        
        df["typical_setup_power(kW)"] = [ flat_typical_power_mean[x[0]] for x in sorted_tuple ]
        df["typical_setup_power_error(kW)"] = [ flat_typical_power_std[x[0]] for x in sorted_tuple ]
        
        df["may_be_multilinear"] = [ flat_is_multilinear[x[0]] for x in sorted_tuple ]
        df["microinverter"] = [ flat_is_microinverter[x[0]] for x in sorted_tuple ]

        df["inverter_model"] = [ QuoteComparison.unHashThis(x[0])[0] for x in sorted_tuple ]
        df["pv_model"] = [ QuoteComparison.unHashThis(x[0])[1] for x in sorted_tuple ]
        df["contractor"] = [ QuoteComparison.unHashThis(x[0])[2] for x in sorted_tuple ]
        df["num_inverters"] = [ QuoteComparison.unHashThis(x[0])[3] for x in sorted_tuple ]
        df["electric_utility"] = [ QuoteComparison.unHashThis(x[0])[4] for x in sorted_tuple ]

        #Remove elements that have uncertainty on regression greater than error_filter
        # df = df[ (df["errorUp_on_cost"]/df["cost_for_%skW"%power]) < error_filter]
		#Remove elements that don't have expereience building this size of solar project (within 2 sigma)
        df = df[( (power < (df["typical_setup_power(kW)"] 
                         + 2*df["typical_setup_power_error(kW)"])) & (power > (df["typical_setup_power(kW)"] 
                         - 2*df["typical_setup_power_error(kW)"]))) ]

        del df["may_be_multilinear"]
        # del df["naive_M$/kW_error"]
        df.rename(columns={'naive_M$/kW':'avg_$/W'}, inplace=True)
        df["error"] = df.errorUp_on_cost.map(lambda x : x*1000)
        df["percent_error"] = (abs(df["errorUp_on_cost"]/df["cost_for_%skW"%power]) + abs(df["errorDown_on_cost"]/df["cost_for_%skW"%power]))/2

        df["avg_$/W"] = df["avg_$/W"].map(lambda x : x*1000)
        del df["errorDown_on_cost"]
        del df["cost_for_%skW"%power]


        return df


