import matplotlib.pyplot as plt # plotting
import matplotlib# plotting
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns


def countplots(df,countable_variables="Default",log_scale="Default",max_categories=7,height=50):
	if countable_variables == "Default":
		countable_variables = ["Community Distributed Generation","Solicitation","Sector","Electric Utility","Affordable Solar","Purchase Type","Primary Inverter Manufacturer","Contractor"]
	if log_scale == "Default":
		log_scale = ["Solicitation","Affordable Solar"]
	plt.rc('axes', labelsize=20)
	# f, axes = plt.subplots(3,3)
	# fig = plt.figure()
	fig, ax = plt.subplots(len(countable_variables),1)
	fig.set_size_inches((12,height))

	for n,variable in enumerate(countable_variables):
	#     print(n)
	#     print(ax[0,0]) 
	    #Look only at the top 7 categories
		p = sns.countplot( y=variable,data=df, ax=ax[n],order=pd.value_counts(df[variable]).iloc[:max_categories].index )
		if variable in log_scale:
			p.set(xscale="log")
	plt.show()
	return 0

def scatterplots(df,main_var="Total Inverter Quantity",other_vars="Default"):
	if other_vars == "Default":
		other_vars = ["Project Cost","Expected KWh Annual Production","Total PV Module Quantity","$Incentive"]

	fig, axs = plt.subplots(int(len(other_vars)/2),2)
	fig.set_size_inches((12,12))
	for y,ax in zip(other_vars,axs.flat):
		ax.set_xlabel(main_var, fontsize=12)
		ax.set_ylabel(y, fontsize=12)
		ax.set_title(main_var +" Vs. "+y , fontsize=14)
		ax.scatter(df[main_var],df[y],s=2.2,marker="+")

	return 0


