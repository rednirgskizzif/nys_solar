

# from sklearn.mixture import GaussianMixture
from sklearn import linear_model
# import matplotlib.pyplot as plt # plotting
# import matplotlib# plotting

import numpy as np # linear algebra
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# pd.set_option('display.max_columns', 999)
# from pprint import pprint
# import seaborn as sns
# import pdb
import statsmodels.api as sm

from QuoteComparison import QuoteComparison
from ProjectQuote import ProjectQuote
import pdb


#initialize model

def load_data():
	df = pd.read_csv('data/solar-electric-programs-reported-by-nyserda-beginning-2000.csv', delimiter=',')

	df = df[df["Affordable Solar"] == "No"]
	df = df[df["Sector"] == "Residential"]
	df = df[df["Community Distributed Generation"] == "No"]
	df = df[df["Purchase Type"] == "Power Purchase Agreement"]
	df = df[df["Project Status"] == "Complete"]

	#Change Prices to Millions of Dollars
	df["Project Cost"] = df["Project Cost"].map(lambda x: x/1000000)
	df["Before Incentive Project Cost"] = (df["Project Cost"] - df["$Incentive"]).map(lambda x: x/1000000)

	df["Expected MWh Annual Production"] = df["Expected KWh Annual Production"].map(lambda x: x/1000)

	del df["Expected KWh Annual Production"]

	df["MicroInverter"] = ["Enphase Energy" in str(x) for x in list(df["Primary Inverter Manufacturer"].values) ]
	df["Cost_per_kW"] = df["Project Cost"] / df["Total Nameplate kW DC"]
	df["Power_per_PV"] = df["Total Nameplate kW DC"] / df["Total PV Module Quantity"]
	df.loc[ df.MicroInverter , "Total Inverter Quantity"] = float(-1.0 )
	df["hash"] = (df["Primary Inverter Model Number"].map(str)
              +"_X_"+df["Primary PV Module Manufacturer"]+"_X_"
              +df["Contractor"]+"_X_"+df["Total Inverter Quantity"].map(str)
              +"_X_"+df["Electric Utility"].map(str) )
	for key in df["hash"].unique():
		if len(df[ df.hash == key ]) < 10:
			df = df.loc[~( df.hash == key )]
	return df

def recursive_outlier_removal(dataframe):

    rate = dataframe["Cost_per_kW"]
    num_points = len(rate)
    
    rate_mean = np.mean(rate)
    rate_std = np.std(rate)
    rate_ucl = rate_mean+1.5*rate_std
    rate_lcl = rate_mean-1.5*rate_std
    
    dataframe = dataframe[ ((dataframe["Cost_per_kW"] < rate_ucl) & (dataframe["Cost_per_kW"] > rate_lcl) )]
    
    if num_points != len(dataframe):
        recursive_outlier_removal(dataframe)
    return dataframe

def get_quotes(df):

#Master dictionary to store all the quotes. Will go to the API class later
	cost_per_KW = {}
	# pdb.set_trace()

	for hsh in df.hash.unique():    
		one_hash = recursive_outlier_removal( df[ df.hash == hsh ] )

		if len(one_hash) < 12:
			continue
		if one_hash["Project Cost"].std() == 0.0:
			continue

		rate_mean = one_hash.Cost_per_kW.mean()
		rate_std = one_hash.Cost_per_kW.std()

		typical_power_mean = one_hash["Total Nameplate kW DC"].mean()
		typical_power_std = one_hash["Total Nameplate kW DC"].std()


		Y  = one_hash["Project Cost"]
		X  = one_hash["Total Nameplate kW DC"]
		results = sm.OLS(Y,sm.add_constant(X)).fit()

		cost_per_KW[hsh] = ProjectQuote(hsh,results)
		cost_per_KW[hsh].setNaiveRate( rate_mean, rate_std)
		cost_per_KW[hsh].setTypicalPower( typical_power_mean, typical_power_std)

		#If the rate has a wide ditribution, throw a multi-colinearilty flag
		bimodal_empirical_param = 0.0005
		if rate_std > bimodal_empirical_param:
		    cost_per_KW[hsh].is_multilinear = True


	return cost_per_KW


if __name__ == "__main__":

	QuoteMachine = QuoteComparison( get_quotes( load_data() ) )
	print( QuoteMachine.returnRankedRates(14,asym_conf_int=False).head() )


