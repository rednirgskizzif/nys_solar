# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from flask import request
import Model
import pandas as pd

# pd.options.display.float_format = '{4f}'.format

QuoteMachine = Model.QuoteComparison( Model.get_quotes( Model.load_data() ) )

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Solar Pricing Dashboard"

server = app.server

colors = {
    'background': '#111111',
    'text': '#6a0dad'
}

dataframe_columns = ["dollar_per_watt",
					 "percent_error",
					 "inverter_model",
					 "pv_model",
					 "contractor",
					 "num_inverters",
					 "electric_utility"]



def get_dataframe(power=14, contractors_filter=None, include_microinverter=True,filter_error=False):
    dataframe = QuoteMachine.returnRankedRates( power , asym_conf_int=False )
    if contractors_filter != None:
        dataframe = dataframe[ (dataframe.contractor.isin( contractors_filter ) ) ]
    if not include_microinverter:
        dataframe = dataframe[ ~(dataframe.num_inverters == "-1.0") ]
    if filter_error:
        dataframe = dataframe[ (dataframe.percent_error < 0.1) ]
    return dataframe


def generate_table(dataframe, max_rows=100):
    items = [] 
        
    for i in range(min(len(dataframe), max_rows)):
        temp = [] 
        for col in dataframe_columns:
            if col in ["dollar_per_watt","percent_error"]:
                temp.append( "{0:.2f}".format(dataframe.iloc[i][col]) ) 
            else:
                temp.append(  dataframe.iloc[i][col] )
        items.append(  html.Tr( [html.Td(col) for col in temp] ) ) 

    # items = html.Tr(items)

    return [ html.Tr( [html.Th(col) for col in dataframe_columns]) ] + items

        
DATAFRAME = get_dataframe()

contractors = DATAFRAME.contractor.unique()

contractor_options = [{'label': con, 'value':con} for con in contractors]


app.layout = html.Div(children=[

    # html.Label('Power Selected : {0}'.format(3)),

    html.H1(
        children='Simple Business Analytics Dashboard',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.Div(children=['Model predictions of residential solar projects less than 25kW \
        utilizing a PPA in the state of New York.',
        ' The model is outlined in the ',html.A('documentation.', href='http://www.simplydylan.me/solarDoc.pdf'),
        ' It should be pointed out that each datum represents a unique solar project architecture and does not represent relative abundance.'],
     style={
        'color': colors['text'],
        'maxWidth': 800,
        'minWidth': 100,
        'marginLeft': 'auto',
        'marginRight': 'auto',
        'textAlign': 'center',
    }),

    html.Div( 
        children=[
        html.H3('Contractors'),
        dcc.Checklist(id='my-contractors',
            options=contractor_options
        # value=['MTL']
        ),
        html.H3('Include Micro Inverter'),
        dcc.RadioItems(id='my-inverter',
            options=[{'label': "True", 'value':1}, 
                    {'label': "False", 'value':0}], 
             value=1
        ),
        html.H3('Remove Large Error'),
        dcc.RadioItems(id='my-error-filter',
            options=[{'label': "True", 'value':1}, 
                    {'label': "False", 'value':0}],
             value=0
            ) 
                ],style={"columns":"2"}
        ),
    html.Div( children=[
        html.H3('Project Total Power (kW DC)'),
        dcc.Slider(
            id='my-power',
            min=0,
            max=15,
            step=0.2,
            marks={i: 'Power: {}'.format(i) if i == 1 else str(i) for i in range(1, 15)},
            value=14,)
              ]
    ),

    dcc.Graph(
        id='data-histogram',
        figure={
            'data': [
                {
                    'x': DATAFRAME[ DATAFRAME.num_inverters == "1.0" ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Single inverter',
                    'type': 'histogram'
                },
                {
                    'x': DATAFRAME[ DATAFRAME.num_inverters == "-1.0" ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Micro-inverters',
                    'type': 'histogram'
                },
                {
                    'x': DATAFRAME[ (DATAFRAME.num_inverters != "1.0") & (DATAFRAME.num_inverters != "-1.0") ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Multiple traditional inverters',
                    'type': 'histogram'
                }
            ],
            'layout': { 'xaxis' : {"title": "$/W","range":[2,7]} }
 
        }
    ),

    html.Table( id="data-table" , children=generate_table( DATAFRAME ) )

])


@app.callback(
    [Output('data-table', 'children'),Output('data-histogram', 'figure')],
    [Input('my-power', 'value'),
    Input('my-contractors', 'value'),
    Input('my-inverter', 'value'),
    Input('my-error-filter', 'value')])
def update_table(power,contractors,microinverter,error_filter):
    df = get_dataframe( power,contractors,microinverter,error_filter )
    hist_dict = {
            'data': [
                {
                    'x': df[ df.num_inverters == "1.0" ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Single inverter',
                    'type': 'histogram'
                },
                {
                    'x': df[ df.num_inverters == "-1.0" ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Micro-inverters',
                    'type': 'histogram'
                },
                {
                    'x': df[ (df.num_inverters != "1.0") & (df.num_inverters != "-1.0") ]['dollar_per_watt'],
                    'nbinsx':25,
                    # 'text': df['STRCITY'],
                    # 'customdata': DATAFRAME['dollar_per_watt'],
                    'name': 'Multiple traditional inverters',
                    'type': 'histogram'
                }
            ],
            'layout': { 'xaxis' : {"title": "$/W","range":[2,7] } }


        }

    return html.Table( id="data-table" , 
        children=generate_table( df ) ), hist_dict

# @app.route('/doc')
# def doc():
#     return url_for('doc.pdf')


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=65000,debug=False)
    # app.run(host='0.0.0.0', port=65000,debug=False)
