import numpy as np

class ProjectQuote(object):
    def __init__(self, project_hash, regressor):
        self.hash = project_hash
        self.inverter_model = project_hash.split("_X_")[0]
        self.PV_model = project_hash.split("_X_")[1]
        self.contractor = project_hash.split("_X_")[2]
        self.inv_qty = project_hash.split("_X_")[3]
        self.utility = project_hash.split("_X_")[4]
        self.regressor = regressor
        
        self.naive_rate_mean = None
        self.naive_rate_std = None
        self.typical_power_mean = None
        self.typical_power_std = None
        
        self.is_multilinear = False
        self.is_microinverter = None
        self.power = None
        self.sys_up = None
        self.sys_down = None

    def getQuote(self,power,assymetrical_conf_int=False):
        self.power=power
        nominal = (power*self.regressor.params[1] + self.regressor.params[0])
        if assymetrical_conf_int:
            try:
                m_up = nominal - (power * (self.regressor.conf_int().loc["Total Nameplate kW DC"][1]) + self.regressor.params[0])
                b_up = nominal - (power * self.regressor.params[1] + (self.regressor.conf_int().loc["const"][1] ))
                m_down = nominal - (power * ( self.regressor.conf_int().loc["Total Nameplate kW DC"][0] ) + self.regressor.params[0])
                b_down = nominal - (power * self.regressor.params[1] + self.regressor.conf_int().loc["const"][0] )
                error_up = np.sqrt(m_up*m_up + b_up*b_up)
                error_down = np.sqrt(m_down*m_down + b_down*b_down)
            except ValueError:
                print("Error in calculating error for hash : ")
                print(self.hash)
                error_up = np.nan
                error_down = np.nan
        else:
            m_up = nominal-(power* (self.regressor.params[1]+self.regressor.bse["Total Nameplate kW DC"]) + self.regressor.params[0])
            b_up = nominal-(power*self.regressor.params[1] + (self.regressor.params[0]+self.regressor.bse["const"]))
            error = np.sqrt(m_up*m_up + b_up*b_up)
            error_up = error
            error_down = error

        return  nominal,error_up,error_down
    
    #Default error on hours of sunlight in new york: 17%. This directly impacts Expected Annual kWh.
    def setSysError(self,error=0.17):
        nom_up, dummy1, dummy2 = self.getQuote(self.power*(1+error))
        nom_down, dummy1, dumm2 = self.getQuote(self.power*(1-error))
        self.sys_up = nom_up
        self.sys_down = nom_down
    
    def setNaiveRate(self,mean,std):        
        self.naive_rate_mean = mean
        self.naive_rate_std = std
        
    def setTypicalPower(self,mean,std):        
        self.typical_power_mean = mean
        self.typical_power_std = std
        
    def getNaiveRate(self):
        return self.naive_rate_mean, self.naive_rate_std 
    
    def getTypicalPower(self):
        return self.typical_power_mean, self.typical_power_std
    
    def getSysError(self):
        return self.sys_up, self.sys_down
    