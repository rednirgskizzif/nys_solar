This is a repository of NY state solar project data exploration. 

To run install miniconda & jupyter:
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh 
	conda install jupyter

Launch PPA\_XPloration.ipnb, install a few basic scipy modules and go. 


Plan is to do data xploration, then solve a couple question, maybe do some light regression, then for fun see if I can train some ML model to give similar results as simple regression.
Focus should be on uncertainties.
