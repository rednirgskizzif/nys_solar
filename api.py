
from flask import Flask
from flask import request
import Model


QuoteMachine = Model.QuoteComparison( Model.get_quotes( Model.load_data() ) )

app = Flask(__name__)


@app.route('/')
def index():
    return "Welcome to the linear regression backend webservice to use:  \n http://18.188.115.50:63000/linear?power=<0-25>"

@app.route('/linear/')
@app.route('/linear')
def linear():
    power = request.args.get('power')
    output_type = request.args.get('type')

    if output_type == "json":
    	return QuoteMachine.returnRankedRates( float(power), asym_conf_int=False ).to_json()
    else :
    	return QuoteMachine.returnRankedRates( float(power), asym_conf_int=False ).to_csv()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=63000,debug=False)
